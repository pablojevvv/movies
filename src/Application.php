<?php


class Application {

    private $modules = [];

    public function register($module) { // 'template'
        // TODO: sprawdzic czy wybrany modul juz nie istnieje
        $class = "\\Movies\\" . ucfirst($module); // '\Movies\Template'
        $this->modules[strtolower($module)] = new $class(); // $this->modules['template'] = new \Movies\Temaplte();
    }

    public function get($module) {
        return $this->modules[$module];
    }

}
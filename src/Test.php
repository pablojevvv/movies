<?php


class Test
{
    private $body;

    public function get($file) {
        $this->body = file_get_contents('../views/index.html');
        return $this;
    }

    public function strong() {
        $this->body = '<strong>' . $this->body . '</strong>';
        return $this;
    }

    public function show() {
        echo '<pre>' . $this->body . '</pre>';
        return $this;
    }

}
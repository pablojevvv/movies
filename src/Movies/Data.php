<?php

namespace Movies;


class Data {

    private $content; // powinien być już sparsowanym JSONem

    public function load($file) {
        // metoda, ktora pobiera zawartosc pliku $file
        // oraz zapisuje tresc pliku $file w poli $content
        // metoda powinna zwracać referencje do swojej instancji (return $this)
        $this->content = json_decode(file_get_contents(__DIR__  . '/../../data/' . $file), true);
        return $this;
    }

    public function getByKey($key, $value) {
        // metoda zwracajaca wybrany rekord z pliku JSON
        // po wybranym kluczu rownym wartosci przekazanej jako argument
        $ret = [];
        foreach($this->content as $row) {
            if($row[$key] == $value) {
                //return $row;
                $ret[] = $row;
            }
        }
        switch(count($ret)) {
            case 0:
                return null;
                break;
            case 1:
                return $ret[0];
                break;
            default:
                return $ret;
        }
    }

}
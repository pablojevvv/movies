<?php
/**
 * Created by PhpStorm.
 * User: acer1
 * Date: 2017-03-10
 * Time: 18:17
 */

namespace Movies;


class Template {
    private $ext = 'html';

    public function printMessage($message) {
        echo '<strong>' . $message . '</strong>' . PHP_EOL;
    }

    public function getTemplate($template) {
        $path = __DIR__ . "/../../views/" . $template . '.' . $this->ext;
        if(file_exists($path)) {
            return file_get_contents($path);
        }
        return null;
    }

    public function getPage($page) {
        return $this->getTemplate('header') . PHP_EOL . $this->getTemplate($page) . PHP_EOL . $this->getTemplate('footer');
    }

    public function fill($content, $array) {
        // metoda powinna zamienic wszystkie wystawpienia
        // { klucz } na wartości przypisane do tego klucza
        // a nastepnie zwrocic uzupelniony juz wartosciami $content
        // jezeli nie bedzie danego klucza w tablicy, to metoda powinna podmienic { __ } na pouste znaki
    }
}
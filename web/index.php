<?php

require_once './../vendor/autoload.php';

$app = new \Application();

$app->register('template');
$app->register('data');

$movie = $app->get('data')->load('movies.json')->getByKey('tags', 'escape|drama|redemption');
print_r($movie);

$action = (isset($_GET['page'])) ? basename($_GET['page']) : 'index';
echo $app->get('template')->getPage($action);


